# Sensor array processor. 

This is a simple Arduino Sketch to include the MKR-ENV shield, read data and 
write it to SD in a specific format. An effort is also done to write to screen
but this was mostly because I needed some debugging info because of the Builtin
led driving me batty and the fact that I left a 


    while(!Serial);


in there. The code won't pass this if there's no port opened. Useful while one's project 
is attached to a computer, but don't forget to remove it.... 

The classes in the code are little more than C-datastructures. (I'm a C-developer, sue me.) 
I had little use for private values and public methods, I just needed to pass references to 
data between functions in a non-exhausting way. 

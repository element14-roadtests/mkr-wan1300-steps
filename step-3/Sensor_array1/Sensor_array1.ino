/*********************************************************
* Sensor Array script, reading the parameters from a MKRWAN 
*  board and storing them to SD. The entire process is done
*  in Arduino sketch, but each step is non-blocking 
*  by using a state-machine. 
* 
* 
*********************************************************/

/***********************************
 * Includes: 
 */
#include <Arduino_MKRENV.h>
#include <SPI.h>
#include <SD.h>

/***********************************
 * Defines: 
 */
#define MEASUREMENT_INTERVAL_SECS 60UL        // Measurement interval in seconds. 
#define BLINK_INTERVAL_MSECS      200UL       // Short blink-on interval in milliseconds. 
#define BLINK_INTERVAL_IDLE_MS    5000UL      // Short blink-on interval in milliseconds. 
#define LED_OUTPUT_PIN            5           // Replacement for the builtin led, which can't be used. 
#define SD_SPI_CS_PIN             4           // Chip select pin for SPI I/O to SD card. 

#define LOG_FILE_NAME             "data.log"  // File to write to SD card. 


/***********************************
 * Enums and classes / data structures: 
 */
// Application FSM state: 
enum AppState {
  StateIdle,      // Do nothing until next interval
  StateMeasuring, // Measurement phase
  StateStoring,   // data store phase. 
};


// Data structure for current measurements: 
class Measurements {
  public: 
    float temperature;
    float humidity;
    float pressure;
    float illuminance;
    float uva;
    float uvb;
    float uvIndex; 
    bool  measurement_valid; 
};


// Application control structure. 
class AppControl {
  public: 
    AppState      state;
    Measurements  measurements;

    unsigned long secs_last_measurement;

    unsigned long ts_last_blink;
    bool          builtin_led_state;
};


/***********************************
 * Application properties: 
 */

AppControl  appControl;
File        logFile;


/***********************************
 * error_output: Output error message to terminal. if crash is true: This function doesn't return and the application hangs. 
 *  Reports in the template: 
 *    
 *    "[module]: [message]\n" 
 *    
 *  Input:  
 *    char *module: Name of the module that reports the error. 
 *    char *msg: Message from the module. 
 *    bool crash: true: Hang the execution. false: return after reporting. 
 */
void error_output(char *module, char *msg, bool crash)
{
  Serial.print("[");
  Serial.print(module);
  Serial.print("]");
  Serial.print(": ");
  Serial.println(msg);
  
  while(crash);
}


/**
 * Blink the led a couple of time in rapid succession. 
 *  This is a blocking subroutine. 
*/
void initial_blink()
{
  for(int i = 0; i < 3; i++)
  {
    digitalWrite(LED_OUTPUT_PIN, HIGH);
    delay(500);  
    digitalWrite(LED_OUTPUT_PIN, LOW);
    delay(500);
  }  
}


/**
 * setup: Program start. Initialize serial, env shield and SD card. 
 */
void setup() 
{
  Serial.begin(9600);

  // Initial blink to report pins are initialized and we're starting hardware init. 
  delay(5000);
  
  Serial.print("Initializing GPIO....");
  pinMode(LED_OUTPUT_PIN, OUTPUT);
  digitalWrite(LED_OUTPUT_PIN, LOW);
  Serial.println("Done.");
  
  initial_blink();
  Serial.print("Initializing appControl....");
  
  appControl.state                          = StateIdle;
  appControl.secs_last_measurement          = 0;
  appControl.ts_last_blink                  = 0;
  appControl.measurements.measurement_valid = false;

  Serial.println("Done.");

  Serial.print("Initializing ENV shield sensors....");
  // Initialize ENV shield. 
  if(!ENV.begin()) error_output((char *)"ENV", (char *)"Could not initialize ENV board.", true);
  Serial.println("Done.");
  
  // Initialize SD card: 
  Serial.print("Initializing ENV shield SD card....");
  if(!SD.begin(SD_SPI_CS_PIN))   error_output((char *)"SD", (char *)"Could not initialize SD interface.", true);
  Serial.println("Done.");

  // Final blink to tell we're done. 
  initial_blink();
}


/**
 * write_to_file: Write set of measurements to file. Each line starts with the current time millis, then all measurements and ends with a newline. 
 * 
 *  Input: 
 *    File *pLog: Reference to an open file. 
 *    Measurements *pMeasurements: reference to the set of measurements. 
 */
void write_to_file(File *pLog, Measurements *pMeasurements)
{
  pLog->print(millis());
  pLog->print("|");
  pLog->print(pMeasurements->temperature);
  pLog->print("|");
  pLog->print(pMeasurements->humidity);
  pLog->print("|");
  pLog->print(pMeasurements->pressure);
  pLog->print("|");
  pLog->print(pMeasurements->illuminance);
  pLog->print("|");
  pLog->print(pMeasurements->uva);
  pLog->print("|");
  pLog->print(pMeasurements->uvb);
  pLog->print("|");
  pLog->println(pMeasurements->uvIndex);
}


/**
 * write_to_screen: Dump all values to terminal. Useful for debugging and developing. 
 *  This function is bacially write_to_file, but to the Terminal instead. 
 * 
 *  Input: 
 *    Measurements *pMeasurements: Measurements to write to terminal. 
*/
void write_to_screen(Measurements *pMeasurements)
{
  Serial.print(millis());
  Serial.print("|");
  Serial.print(pMeasurements->temperature);
  Serial.print("|");
  Serial.print(pMeasurements->humidity);
  Serial.print("|");
  Serial.print(pMeasurements->pressure);
  Serial.print("|");
  Serial.print(pMeasurements->illuminance);
  Serial.print("|");
  Serial.print(pMeasurements->uva);
  Serial.print("|");
  Serial.print(pMeasurements->uvb);
  Serial.print("|");
  Serial.println(pMeasurements->uvIndex);
}


/**
 * evaluate_led_state: Update builtin led state non-blocking.
 * 
 *  Input: 
 *    AppControl *pAppControl: Reference to app-control.
*/
void evaluate_led_state(AppControl *pAppControl)
{
  // If led is off and last blink was over a minute ago: 
  if(!pAppControl->builtin_led_state && 
    (pAppControl->ts_last_blink == 0 || 
      (pAppControl->ts_last_blink + BLINK_INTERVAL_IDLE_MS) < millis()))
  {
    // switch on led. 
    Serial.println("led on");
    digitalWrite(LED_OUTPUT_PIN, HIGH);
    
    pAppControl->builtin_led_state  = true;
    pAppControl->ts_last_blink      = millis();
  }  
  // else: if led is on and the timestamp is more than 100 ms ago: 
  else if(pAppControl->builtin_led_state && (pAppControl->ts_last_blink + 100UL) < millis())
  {
    // Switch it off. 
    Serial.println("led off");
    digitalWrite(LED_OUTPUT_PIN, LOW);
    pAppControl->builtin_led_state  = false;    
    pAppControl->ts_last_blink      = millis();
  }
}


/**
 * loop: Main application FSM. 
*/
void loop() 
{
  unsigned long time_buffer;

  // Update ledstate non-blocking.
  evaluate_led_state(&appControl);
  
  // Execute FSM: 
  switch(appControl.state)
  {
    case StateIdle:       
      // *** patiently wait, monitoring the time in seconds before next measurement. 
      // *** If the interval has passed, step to StateMeasuring. 
      
      // Get current time in secs.  
      time_buffer = millis() / 1000UL;
      
      // If delta between current time and previous time > interval or or previous time is 0 and current time is 10 seconds: 
      if(appControl.secs_last_measurement == 0 && time_buffer >= 10UL || 
        (time_buffer - appControl.secs_last_measurement) > MEASUREMENT_INTERVAL_SECS)
      {
        // go to StateMeasuring. 
        appControl.state = StateMeasuring;
      }
      
      break; // case StateIdle
      
    case StateMeasuring:  
      // *** Call the API's to fetch all values. 
      // *** once everything is stored, set measurement_valid to true. 
      Serial.print("Measuring....");

      // Write all values to application structure. 
      appControl.measurements.temperature  = ENV.readTemperature();
      appControl.measurements.humidity     = ENV.readHumidity();
      appControl.measurements.pressure     = ENV.readPressure();
      appControl.measurements.illuminance  = ENV.readIlluminance();
      appControl.measurements.uva          = ENV.readUVA();
      appControl.measurements.uvb          = ENV.readUVB();
      appControl.measurements.uvIndex      = ENV.readUVIndex (); 
      
      // Update boolean. 
      appControl.measurements.measurement_valid = true;
      Serial.println("Done.");
      
      // step to storing. 
      appControl.state = StateStoring;
      
      break; // case StateMeasuring
      
    case StateStoring:    
      // *** Write to SD. Set meaurement_valid to false. Update previous_time.
      //  Open store-file
      Serial.print("Storing....");
      logFile = SD.open(LOG_FILE_NAME, FILE_WRITE);

      // Write data. 
      write_to_file(&logFile, &appControl.measurements);
      write_to_screen(&appControl.measurements);
      
      // Close file. 
      logFile.close();
      Serial.println("Done.");
      
      // update measurement_valid, secs_last_measurement and state. 
      appControl.measurements.measurement_valid = true;
      appControl.secs_last_measurement          = millis() / 1000UL;
      appControl.state                          = StateIdle;
      
      break; // case StateStoring
  } // switch(appControl.state)
}

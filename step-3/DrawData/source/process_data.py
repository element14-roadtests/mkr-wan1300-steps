"""
Simple matplotlib script to read the dataset and plot the results. Not very inspired, but
basically takes into account the fields on each line of log and stores them in seperate data lists.
Then each datalist (except the first one) gets plotted against the first one.

The graphs list at the top of this script configures each plot for name, x and y axis and respective
limits.

"""
import matplotlib.pyplot as plt

graphs = [
    {'name': 'temperature', 'y_axis': 'celsius',    'ymin': 10,     'ymax': 60},
    {'name': 'humidity',    'y_axis': 'rH',         'ymin': 0,      'ymax': 100},
    {'name': 'pressure',    'y_axis': 'hPa',        'ymin': 50,     'ymax': 1260},
    {'name': 'illuminance', 'y_axis': 'LUX',        'ymin': 1400,   'ymax': 1700},
    {'name': 'uva',         'y_axis': 'intensity',  'ymin': 60,     'ymax': 150},
    {'name': 'uvb',         'y_axis': 'intensity',  'ymin': -30,    'ymax': 40},
    {'name': 'uvIndex',     'y_axis': '-',          'ymin': -0.5,   'ymax': 1}
]


def process_data(filename: str) -> dict:
    """
    process_data: open the file for reading and parse each line to seperate lists.

    :param filename: filename of the datasource to read and parse.

    :return: A dictionary with the name for each source and then the list. In conjunction with
        the list above this makes it easiest to process.
    """
    # open file.
    print('Reading file: %s....\t ' % filename, end='')
    with open(filename, 'r') as datafile:
        lines = datafile.readlines()

    timestamps = []
    temperatures = []
    humiditys = []
    pressures = []
    illuminances = []
    uvas = []
    uvbs = []
    uvIndexs = []

    # read lines to build datasets.
    for line in lines:
        (timestamp, temperature, humidity, pressure, illuminance, uva, uvb, uvIndex) = line.split('|')
        timestamps.append(int(timestamp) / 1000)
        temperatures.append(float(temperature))
        humiditys.append(float(humidity))
        pressures.append(float(pressure))
        illuminances.append(float(illuminance))
        uvas.append(float(uva))
        uvbs.append(float(uvb))
        uvIndexs.append(float(uvIndex))

    print('Done reading file.')

    return {'timestamp': timestamps, 'temperature': temperatures,  'humidity': humiditys,
            'pressure': pressures, 'illuminance': illuminances, 'uva': uvas, 'uvb': uvbs,
            'uvIndex': uvIndexs}


def draw_line_graph(graphname: str, timestamps: list, values: list, y_axis_name: str, y_min,
                    y_max):
    """
    Draw a single line-graph of timestamps for the x-axis and a set of values for the y-axis.
        The graphname is used for creating the filename and selecting all data from above list.

    :param graphname: name of the graph that is parsed.
    :param timestamps: the list of values in the first column of the dataset.
    :param values: The values to plot in this list.
    :param y_axis_name: label of the y-axis to print.
    :param y_min: lower limit to set the y-axis to.
    :param y_max: high limit to set the y-axis to.
    """
    filename = '%s.png' % graphname

    print('creating %s...\t' % filename, end='')
    print('y_min: %d - y_max: %d' % (y_min, y_max))

    plt.figure()
    plt.ylabel(y_axis_name)
    plt.plot(timestamps, values)
    plt.savefig(filename)

    print('Done.')


def draw_graphs(dataset: dict):
    """
    draw_graphs: Run through the list of sets and for each set: select the data from the dataset
      parameter and call draw_line_graph. This function connects the given dataset to the list at
      the top of the script.

    :param dataset: The set produced with the process_data function.

    """
    print('Drawing graphs....')

    for graph in graphs:
        draw_line_graph(graph['name'], dataset['timestamp'], dataset[graph['name']],
                        graph['y_axis'], graph['ymin'], graph['ymax'])

    print('Done Drawing graphs. Have a nice day.')


def main():
    """
    Main: Point of entry for the script.
    In order, calls:
      - Read the data
      - draw the graphs.
    """
    print('Master, I am here to serve you!')
    result = process_data('DATA.LOG')

    print(result['timestamp'])
    draw_graphs(result)


# Start the application, calling main.
if __name__ == '__main__':
    main()

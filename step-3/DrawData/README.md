# DrawData

A simple python3 script employing matplotlib to read the dataset and plot a given set of graphs. 
This isn't a particularly inspired script and has a lot of coupling with the Sensor_Array1 project 
and its resulting data structure. But it does the trick. 
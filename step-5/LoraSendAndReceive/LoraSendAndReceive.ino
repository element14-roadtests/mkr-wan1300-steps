/*
  Lora Send And Receive
  This sketch demonstrates how to send and receive data with the MKR WAN 1300/1310 LoRa module.
  This example code is in the public domain.
*/

#include <MKRWAN.h>

LoRaModem modem;

#include "arduino_secrets.h" // (Blocked from checking in on git. Sorry. ;-) ) 

// Polling is slightly lower than the connect interval so that, if connected it's always perceived before 
//  sending. This should make sure that after sending, if anything should be there to be received, it should 
//  be in the buffer when we check it. 
#define MODEM_POLL_INTERVAL_SECS         290UL // 4 minutes, 50 seconds. 
#define MODEM_CONNECT_INTERVAL_MSECS  300000UL // 5 minutes
#define MODEM_CONNECT_INTERVAL_MSECS  130000UL // 2 minutes
#define BLINK_INTERVAL_IDLE_MS          5000UL // 5 secs
#define LORA_RECEIVE_BUFFER_SIZE           64


// Application statemachine steps. 
enum AppState {
  StateIdle,
  StateConnect,
  StateSend,
  StateReceive,
  StateDisconnect,
};


// Main control structure. 
class AppControl 
{
  public: 
    unsigned long last_update_millis;
    
    bool      is_connected;
    AppState  state;

    unsigned long ts_last_blink;
    bool          builtin_led_state;
};


// Module / application declarations. 
AppControl  appControl;

// values from the arduino_secrets.h file. 
String      appEui            = SECRET_APP_EUI;       
String      appKey            = SECRET_APP_KEY;       

String      abpDevAddr        = ABP_DEVICE_ADDRESS;   
String      abpDevEUI         = ABP_DEVICE_EUI;       
String      abpNWSessionKey   = NETWORK_SESSION_KEY;
String      abpAppSessionKey  = APP_SESSION_KEY;

/***********************************
 * error_output: Output error message to terminal. if crash is true: This function doesn't return and the application hangs. 
 *  Reports in the template: 
 *    
 *    "[module]: [message]\n" 
 *    
 *  Input:  
 *    char *module: Name of the module that reports the error. 
 *    char *msg: Message from the module. 
 *    bool crash: true: Hang the execution. false: return after reporting. 
 */
void error_output(char *module, char *msg, bool crash)
{
  Serial.print("[");
  Serial.print(module);
  Serial.print("]");
  Serial.print(": ");
  Serial.println(msg);
  
  while(crash);
}


bool connect_next_step(AppControl *pAppControl)
{
  if (!pAppControl->is_connected) 
  {
    error_output("LoRa", "Unable to connect. No signal.", false);
  }
  else
  {
    modem.minPollInterval(MODEM_POLL_INTERVAL_SECS);  
  }
  
  return pAppControl->is_connected;  
}


/**
 * lora_connect: Call connect function. This function simply assumes we're not connected yet and calls joinOTAA. 
 * 
 *  Input: 
 *    AppControl *pAppControl: Reference to the control structure. 
 *    
 *  Output: (bool)true: connection successful. false otherwise. 
*/
bool lora_connect_otaa(AppControl *pAppControl)
{
  Serial.println(" -- OTAA connect -- ");
  pAppControl->is_connected = modem.joinOTAA(appEui, appKey) == 1;
  
  return connect_next_step(pAppControl);
}



/**
 * lora_connect_abp: Activation By Personalisation method of connection. Connect using ABP credentials.
 * 
 *  Input: 
 *    AppControl *pAppControl: Reference to the Application control structure. 
 * 
 *  Output: (bool) Rsult of connection. 
*/
bool lora_connect_abp(AppControl *pAppControl)
{
  Serial.println(" -- ABP connect -- ");
  pAppControl->is_connected = modem.joinABP(abpDevAddr, abpNWSessionKey, abpAppSessionKey);
  
  return connect_next_step(pAppControl);
}


/**
 * display_modem_info: Helper function to dump modem info to the serial terminal. 
*/
void display_modem_info()
{
  // modem info.
  Serial.print("Your module version is: ");
  Serial.println(modem.version());
  Serial.print("Your device EUI is: ");
  Serial.println(modem.deviceEUI());
  
}


/**
 * Blink the led a couple of time in rapid succession. 
 *  This is a blocking subroutine. 
*/
void initial_blink()
{
  for(int i = 0; i < 3; i++)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);  
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
  }  
}



void setup()
{
  // Initialize and wait for serial
  Serial.begin(115200);
  while (!Serial);
  // delay(5000);

  // Initialize low-level IO. 
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  initial_blink();

  // Initialize appControl object.
  appControl.last_update_millis = 0UL;
  appControl.is_connected       = false;
  appControl.state              = StateIdle;

  // Initialize modem: change this to your regional band (eg. US915, AS923, ...)
  if (!modem.begin(EU868)) 
  {
    error_output("LoRa", "Failed to start module.", true);
  }

  display_modem_info();
}


/**
 * evaluate_led_state: Update builtin led state non-blocking.
 * 
 *  Input: 
 *    AppControl *pAppControl: Reference to app-control.
*/
void evaluate_led_state(AppControl *pAppControl)
{
  // If led is off and last blink was over a minute ago: 
  if(!pAppControl->builtin_led_state && 
    (pAppControl->ts_last_blink == 0 || 
      (pAppControl->ts_last_blink + BLINK_INTERVAL_IDLE_MS) < millis()))
  {
    // switch on led. 
    // Serial.println("led on");
    digitalWrite(LED_BUILTIN, HIGH);
    
    pAppControl->builtin_led_state  = true;
    pAppControl->ts_last_blink      = millis();
  }  
  // else: if led is on and the timestamp is more than 100 ms ago: 
  else if(pAppControl->builtin_led_state && (pAppControl->ts_last_blink + 100UL) < millis())
  {
    // Switch it off. 
    // Serial.println("led off");
    digitalWrite(LED_BUILTIN, LOW);
    
    pAppControl->builtin_led_state  = false;    
    pAppControl->ts_last_blink      = millis();
  }
}


/**
 * read_received: Read modem buffer and write result to Terminal.
*/
void read_received()
{
  int   i = 0;
  char  rcv[LORA_RECEIVE_BUFFER_SIZE];
  
  while (modem.available()) 
  {
    rcv[i++] = (char)modem.read();
  }

  if (i == 0)
  {  
    return;  
  }
  
  Serial.print("Received: ");
  
  for (unsigned int j = 0; j < i; j++) 
  {
    Serial.print(rcv[j] >> 4, HEX);
    Serial.print(rcv[j] & 0xF, HEX);
    Serial.print(" ");
  }
  
  Serial.println();  
}


/**
 * Main application loop. This function is called after 'setup' and immediately after it ends.
*/
void loop() 
{  
  unsigned long curr_millis = millis();
 
  int value_to_send;
  int send_res;

  // Execute statemachine. 
  switch(appControl.state)
  {
    case StateIdle: 
      // If interval to connect and send has passed: go to connect step. 
      if(appControl.last_update_millis == 0 || ((curr_millis - appControl.last_update_millis) > MODEM_CONNECT_INTERVAL_MSECS))
      {
        appControl.last_update_millis   = curr_millis;
        appControl.state                = StateConnect;
        
        Serial.print("Connecting.... ");
      }
      
      // ASync blink of status led. 
      evaluate_led_state(&appControl);

      break; // case StateIdle
      
    case StateConnect:    
      // Either we're already OTAA'd, or call connect. if successful go to send. otherwise goto Disconnect (resetting the modem). 
      digitalWrite(LED_BUILTIN, LOW);
      appControl.state = appControl.is_connected || lora_connect_abp(&appControl) ? StateSend : StateDisconnect;
      // appControl.state = lora_connect_abp(&appControl) ? StateSend : StateDisconnect;
      Serial.println(appControl.state == StateSend ? "Connected" : "Failed");
      
      break; 
    
    case StateSend:       
      // At this stage we're connected. Calculate value to send and write to modem. 
      value_to_send = appControl.last_update_millis / 1000UL;
      modem.setPort(3);
      
      Serial.print("Sending value: ");
      Serial.print(value_to_send);
      modem.beginPacket();
      modem.print("TS: ");
      modem.print(value_to_send);
      
      send_res = modem.endPacket(false);

      Serial.print(send_res > 0 ? " - Success" : " - Failed");
      
      Serial.println("!");
      Serial.print("  - send_res: " );
      Serial.println(send_res);
      
      appControl.state = StateReceive;
      
      break; // case StateSend
      
    case StateReceive: 
      // After send, check if polling dropped anything in the buffer. 
      read_received();
      appControl.state = StateDisconnect;
      
      break; // case StateReceive
      
    case StateDisconnect: 
      // If we didn't join OTAArestart and go to idle mode. 
      if(!appControl.is_connected)
      {
        Serial.println("modem restart and to idle.");
        modem.restart();
      }
      
      Serial.println("snore...");
      appControl.state = StateIdle;
      
      break; // case StateDisconnect
  }
}

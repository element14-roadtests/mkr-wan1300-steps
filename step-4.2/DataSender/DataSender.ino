/*****************************************************************
* Data sender script, reading ENV values and a switch to send to 
*   The receiver. It then checks the receive buffer to update 
*   its own led, depending on what the receiver sends. 
*****************************************************************/


/******************************************
* Includes:
*/

#include <SPI.h>
#include <LoRa.h>
#include <Arduino_MKRENV.h>


/******************************************
* Defines:
*/

#define PIN_SWITCH        0
#define PIN_LED           1
#define DATA_BUFFER_SIZE 64


/******************************************
* Class headers:
*/

// Data structure for current measurements: 
class Measurements {
  public: 
    float temperature;
    float humidity;
    float pressure;

    bool  switch_state;
};


/******************************************
* Local Module variables:
*/

Measurements  measurements;
char          data_buffer[DATA_BUFFER_SIZE];
int           prev_index = 0;


/******************************************
* Functions:
*/


/**
* setup: Initial setup of the device and IO. Setup pins for led and the serials for terminal and modem. 
*/
void setup() 
{
  // Setup switch pin. 
  pinMode(PIN_SWITCH, INPUT);
  
  // Setup led pin. 
  pinMode(PIN_LED, OUTPUT);
  
  // Setup serial and wait for it. 
  Serial.begin(9600);
  
  while(!Serial);

  // Start ENV board library. 
  ENV.begin();

  // Start Modem. 
  if (!LoRa.begin(915E6)) 
  {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
  else
  {
    Serial.println("LoRa started.");  
  }
}


/**
* execute_measurements: Update measurements data structure with latest measured values. 
*/
void execute_measurements()
{
  // put your main code here, to run repeatedly:
  Serial.print("Executing measurements...");
  
  measurements.temperature  = ENV.readTemperature();
  measurements.humidity     = ENV.readHumidity();
  measurements.pressure     = ENV.readPressure();  

  Serial.println("Done.");
}


/**
* evaluate_switches: Update measurements structure with newest switch state. 
*/
void evaluate_switches()
{
  measurements.switch_state = digitalRead(PIN_SWITCH);
}


/**
* send_data: Take the entire measurement structure and write it to network. 
*/
void send_data()
{
  Serial.println("Sending data...." );
  // Start nw LoRa message. 
  LoRa.beginPacket();

  // Send message structure. 
  LoRa.print(measurements.temperature);
  LoRa.print(" ");
  LoRa.print(measurements.humidity);
  LoRa.print(" ");
  LoRa.print(measurements.pressure);
  LoRa.print(" ");
  LoRa.print(measurements.switch_state);

  // End LoRa message.
  LoRa.endPacket();
  Serial.println("Done Sending data.");
}


/**
* read_buffer: read the LoRa buffer for available data. write this to data_buffer.
*   
*   Output: (bool)true: There was data to parse. False otherwise. 
*/
bool read_buffer()
{
  // Serial.println("Reading buffer.");
  
  bool  data_available  = false;
  int   index           = 0;
  int   packetSize      = LoRa.parsePacket();
  
  if (!packetSize) 
  {
    return false;
  }
    
  while (LoRa.available() && index < (DATA_BUFFER_SIZE - 2)) 
  {
    data_buffer[index++]  = (char)LoRa.read();
    data_available        = true;
  }

  data_buffer[index] = '\0';
  // Serial.print("Done reading buffer: ");
  
  if(data_available)
  {
    Serial.println(data_buffer);
  }
//  else
//  {
//    Serial.println("No data available");
//  }
  
  return data_available;
}


/**
* process_buffer: Read received data and try to parse it.
*   It's a 2 value string with one space.
*   Element1: a unique index (or in the case of an actual update, at least not the same as the previous one) 
*   Element2: The new state of the switch on the receiver. 
*/
void process_buffer()
{
  Serial.println("Processing buffer");
  
  // Buffer structure: 
  if(strchr(data_buffer, ' ') == NULL)
  {
    return;  
  }

  char *data_index  = strtok(data_buffer, " ");
  char *value       = strtok(NULL, " ");
  int   nDataIndex  = atoi(data_index);
  int   nValue      = atoi(value);

  Serial.print("index: ");
  Serial.print(nDataIndex);
  Serial.print(" - value: ");
  Serial.println(nValue);
  
  if(nDataIndex == prev_index)
  {
    return;  
  }
  
  int new_value = nValue == 1 ? HIGH : LOW;
  digitalWrite(PIN_LED, new_value);
  prev_index = nDataIndex;
}


/**
* loop: Execute measurements, evaluate switches and send the data. Check for updates and 
*   process those. 
*/
void loop() 
{
  // execute measurements. 
  execute_measurements();
  evaluate_switches();

  // Send data to receiver. 
  send_data();
  
  // Read buffer for updates. 
  if(read_buffer())
  {
    // Process updates. 
    process_buffer();
  }

  unsigned long curr_millis = millis();
  while(millis() < curr_millis + 100)
  {
    if(read_buffer())
    {
      process_buffer();  

      break;
    }
  }
  
  // wait for 20 seconds. 
  delay(20000);
}

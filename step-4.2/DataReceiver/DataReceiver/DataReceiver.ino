/******************************************************
* Data receiver, written as counterpart to the step-4.2
*  sender. Monitors the air for updates, and sends updates
*  of its own if a switch is changed. 
******************************************************/

/*****************************************
* Includes: 
*/
#include <LoRa.h>


/*****************************************
* Defines: 
*/

#define DATA_BUFFER_SIZE 64
#define PIN_SWITCH        5


/*****************************************
* Module variables: 
*/

bool  initial_switchstate_sent  = false;
bool  switchstate               = false;
int   packet_index              = 1;
char  data_buffer[DATA_BUFFER_SIZE];



/*****************************************
* Functions: 
*/

/**
* setup: 
*   Setup pinModes. 
*   Setup Serial to modem and terminal.
*/
void setup() 
{
  
  pinMode(PIN_SWITCH, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  Serial.begin(9600);
  while(!Serial);

  Serial.println("LoRa Receiver");

  if (!LoRa.begin(915E6)) 
  {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}


/**
* read_buffer: Check LoRa modem buffer for data. Write this to the local data_buffer. 
* 
*   Output:(bool) true: Data received. false: No data available. 
*/
bool read_buffer()
{
  bool  data_available  = false;
  int   index           = 0;
  
  int packetSize = LoRa.parsePacket();
  
  if (packetSize) 
  {  
    while (LoRa.available()) 
    {
      data_buffer[index++]  = (char)LoRa.read();
      data_available        = true;
    }

    Serial.print("Packet received with RSSI ");
    Serial.println(LoRa.packetRssi());    
  }

  data_buffer[index] = '\0';

  return data_available;
}

/**
* output_buffer: Write data_buffer to terminal. 
*/
void output_buffer()
{
  Serial.print("Message: ");
  Serial.println(data_buffer);
}


/**
* evaluate_switch_state: read sender's switch-state and if it either changed or 
*   we have not sent anything yet to begin with, send the new state. 
*/
void evaluate_switch_state()
{
  int curr_switchstate = digitalRead(PIN_SWITCH);

  if(curr_switchstate != switchstate || !initial_switchstate_sent)
  {
    Serial.print("Sending new switch-state:");
    Serial.println(curr_switchstate);
    
    LoRa.beginPacket();
    LoRa.print(packet_index);
    LoRa.print(" ");
    LoRa.print(curr_switchstate);
    LoRa.endPacket();    

    switchstate               = curr_switchstate;
    initial_switchstate_sent  = true;
    packet_index             += 1;
  }
}


/**
* Main loop. Read the buffer, evaluate switch state and send new state to the other side. 
*   Then wait a while. 
*/
//void loop() 
//{
//  // Read buffer. 
//  Serial.print("Reading buffer.... "); 
//  
//  if(read_buffer())
//  {
//    // Write to terminal. 
//    output_buffer();
//  }
//  else
//  {
//    Serial.println("No data received.");  
//  }
//  
//  // if switch is changed, send update. 
//  Serial.println("Evaluating switch state.");
//  // evaluate_switch_state();
//
//  // wait. 
//  Serial.println("Waiting 20 seconds....");
//  delay(20000);
//}

void loop() 
{
  if(read_buffer())
  {
    output_buffer();
    evaluate_switch_state();
  }
}

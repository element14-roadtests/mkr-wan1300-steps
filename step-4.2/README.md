# Roadtest step 4.2

This step in the test is actually an expansion on step 4. In step 4 we've done a simple Sender / Receiver peer to peer setup in which the receiver monitors for messages, and the sender sends them once every interval. While this proves the connection, the result was... Underwhelming. 

This step takes the ENV board and periodically reads several of the values and then sends them in a structure. 
Both the sender and receiver board have a switch. The sender board also has a LED. The sender sends a message and then proceeds to monitor the frequency for a response. The receiver in turn will receive the message, and knowing the sender will listen for a while, will proceed to send its switch state if it was changed since last interval. (Or if the switch has not been sent this life-cycle of the receiver). 

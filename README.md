# MKR WAN1300-steps

This project contains all my roadtest steps for the MKR WAN1300 roadtest. The code is referenced by url from the review itself. 

## Steps: 

I've split up all the test steps in their seperate directories. 

- step-2: Blinkenlight: A clone of the tutorial / sample code, with the timing changed. 

- step-3: Read sensors: Using the provided API, access the sensors, read them and write them to SD. A second project is a parser that reads the datafile and creates graphs. 

- step-4: Simple communication: Peer to peer communication, to get a feel of communication between these modules, to prove the antenna's work and that I have not in fact ruined / burned the modems in step 3... 

- step-4.2: Added this extra step because step 4 both actually worked and felt too simple. This project feels slightly more complex and challenging, while still easy to reproduce.

- Step-5: Connect to the actual LoRa-WAN network!
